from functools import reduce

class ZeroDivision(Exception): pass
class NegativeRoot(Exception): pass
  
# Functions

def divi(n, n2):
  return float((n - n % n2) / n2)
  
def div(n, n2):
  if n == 0 or n2 == 0:
    raise ZeroDivision
  return float(divi(n, n2) + 1 / n2)
  
def sroot(n):
  if n < 0: raise NegativeRoot
  return float(n**.5)
  
def croot(n):
  if n < 0: raise NegativeRoot
  return float(n**div(1, 3))
  
def root(a, n):
  if n < 0: raise NegativeRoot
  return float(n**div(1, a))
  
def powr(ns):
  def power(a, b):
    res = 1
    for _ in range(b):
        res *= a
    return res
  return reduce((lambda x, y: power(x, y)), ns)
  
# Class Caulator | Problem Solver

class Caulator:
  def __init__(self, p:str):
    self.p = p
  @property
  def __lexed__(self):
    symbols = [
      '+', '-', 'x', '/',
      '^', 'sqrt', 'cbrt',
      '!', '(', ')'
    ]
    lexed, t = (), ''
    for c in self.p.replace(' ', ''):
      if c in symbols:
        t += c
      else:
        lexed += (t, c)
        t = ''
    return lexed
